package com.srdjan.android.ui.base;

import androidx.fragment.app.Fragment;

import com.srdjan.android.R;

public class BaseFragment extends Fragment {

    protected void switchToFragment(Fragment currentFragment, Fragment nextFragment) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .hide(currentFragment)
                .add(R.id.fragmentContainer, nextFragment)
                .addToBackStack(null)
                .commit();
    }

}
