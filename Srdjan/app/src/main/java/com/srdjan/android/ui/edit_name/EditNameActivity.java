package com.srdjan.android.ui.edit_name;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.srdjan.android.R;
import com.srdjan.android.common.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditNameActivity extends AppCompatActivity {

    @BindView(R.id.etName)
    EditText etName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_name);
        ButterKnife.bind(this);

        // Receive NAME from MainActivity through Intent
        String name = getIntent().getStringExtra(Constants.Extras.NAME);

        if (name != null) {
            // If name is non-null, display it
            updateUi(name);
        } else {
            // Else display hint in EditText (Action Bar will show the default name - name of the app)
            etName.setHint(R.string.default_name);
        }
    }

    private void updateUi(String name) {
        getSupportActionBar().setTitle(name);
        etName.setText(name);
    }

    @OnClick(R.id.btnSave)
    public void onBtnSave() {
        // Send updated name back to MainActivity as result
        Intent data = new Intent();
        data.putExtra(Constants.Extras.NAME, etName.getText().toString());
        setResult(RESULT_OK, data);
        finish();
    }

}
