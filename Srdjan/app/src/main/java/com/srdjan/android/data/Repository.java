package com.srdjan.android.data;

import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import com.srdjan.android.data.local.LocalDataSource;
import com.srdjan.android.data.local.LocalDataSourceImpl;
import com.srdjan.android.data.remote.RemoteDataSource;
import com.srdjan.android.data.remote.RemoteDataSourceImpl;
import com.srdjan.android.data.remote.model.ImageModel;

import java.util.List;

public class Repository implements LocalDataSource, RemoteDataSource {

    private LocalDataSource localDataSource = new LocalDataSourceImpl();
    private RemoteDataSource remoteDataSource = new RemoteDataSourceImpl();

    @Override
    public void setName(String name) {
        localDataSource.setName(name);
    }

    @Nullable
    @Override
    public String getName() {
        return localDataSource.getName();
    }

    @Override
    public void getAllImages(Consumer<List<ImageModel>> onSuccess, Consumer<Throwable> onError) {
        remoteDataSource.getAllImages(onSuccess, onError);
    }

}
