package com.srdjan.android.ui.base;

import android.content.Context;

public interface BaseContract {

    public interface View {
        Context getContext();
    }

    public interface Presenter {

    }

}
