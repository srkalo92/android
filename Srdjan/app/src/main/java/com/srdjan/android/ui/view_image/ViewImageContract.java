package com.srdjan.android.ui.view_image;

import android.os.Bundle;

import com.srdjan.android.data.remote.model.ImageModel;
import com.srdjan.android.ui.base.BaseContract;

public interface ViewImageContract {

    public interface View extends BaseContract.View {
        void showImage(ImageModel image);

        void showImageError(String error);
    }

    public interface Presenter extends BaseContract.Presenter {
        void setImageModel(ImageModel imageModel);

        void shareImage();

        void shareUrl();
    }

}
