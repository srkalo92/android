package com.srdjan.android.data.remote;

import androidx.core.util.Consumer;

import com.srdjan.android.data.remote.model.ImageModel;

import java.util.List;

public interface RemoteDataSource {

    void getAllImages(Consumer<List<ImageModel>> onSuccess, Consumer<Throwable> onError);

}
