package com.srdjan.android.common.listeners;

public interface OnItemClickListener<T> {
    void onItemClick(T item);
}
