package com.srdjan.android.ui.gallery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.srdjan.android.R;
import com.srdjan.android.data.remote.model.ImageModel;
import com.srdjan.android.ui.base.BaseFragment;
import com.srdjan.android.ui.gallery.list.GalleryAdapter;
import com.srdjan.android.ui.view_image.ViewImageFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class GalleryFragment extends BaseFragment implements GalleryContract.View, SwipeRefreshLayout.OnRefreshListener {

    private final GalleryContract.Presenter mPresenter = new GalleryPresenter(this);

    @BindView(R.id.tvError)
    TextView tvError;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private GalleryAdapter adapter;

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);
        ButterKnife.bind(this, view);
        adapter = new GalleryAdapter(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        swipeRefresh.setOnRefreshListener(this);
        mPresenter.loadGallery();
        return view;
    }

    @Override
    public void onRefresh() {
        mPresenter.loadGallery();
        swipeRefresh.setRefreshing(true);
    }

    @Override
    public void showImages(List<ImageModel> images) {
        swipeRefresh.setRefreshing(false);
        tvError.setVisibility(View.GONE);
        adapter.setOnItemClickListener(item -> switchToFragment(this, ViewImageFragment.newInstance(item)));
        adapter.setData(images);
    }

    @Override
    public void showImagesError(String error) {
        swipeRefresh.setRefreshing(false);
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(R.string.error_loading_gallery);
        Timber.e(error);
    }

}
