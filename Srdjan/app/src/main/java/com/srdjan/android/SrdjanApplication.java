package com.srdjan.android;

import android.app.Application;

import timber.log.Timber;

public class SrdjanApplication extends Application {

    private static Application instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Timber.plant(new Timber.DebugTree());
    }

    public static Application getInstance() {
        return instance;
    }

}
