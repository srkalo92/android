package com.srdjan.android.ui.gallery.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.srdjan.android.R;
import com.srdjan.android.common.listeners.OnItemClickListener;
import com.srdjan.android.data.remote.model.ImageModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryViewHolder> {

    public class GalleryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTitle)
        public TextView tvTitle;

        @BindView(R.id.ivImage)
        public ImageView ivImage;

        public GalleryViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(mData.get(getAdapterPosition()));
                }
            });
        }
    }

    private final Context mContext;
    private List<ImageModel> mData = new ArrayList<>();
    private OnItemClickListener<ImageModel> onItemClickListener;

    public GalleryAdapter(Context context) {
        mContext = context;
    }

    public void setData(List<ImageModel> data) {
        mData = data;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener<ImageModel> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public GalleryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_list_item, parent, false);
        return new GalleryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryViewHolder holder, int position) {
        ImageModel item = mData.get(position);
        holder.tvTitle.setText(item.author);
        Glide.with(mContext)
                .load(item.download_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .into(holder.ivImage);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

}
