package com.srdjan.android.common;

public class Constants {

    public static final String BASE_URL = "https://picsum.photos";

    public static class Prefs {
        public static final String NAME = "name";
    }

    public static class Extras {
        public static final String NAME = "name";
    }

    public static class RequestCodes {
        public static final int GET_NAME = 0x2842; // Should be as unique as possible;
    }

}
