package com.srdjan.android.data.remote;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;

import com.srdjan.android.common.Constants;
import com.srdjan.android.data.remote.model.ImageModel;

import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class RemoteDataSourceImpl implements RemoteDataSource {

    private final Retrofit retrofit;
    private final RetrofitService service;

    public RemoteDataSourceImpl() {
        retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(RetrofitService.class);
    }

    public interface RetrofitService {
        @GET("v2/list")
        Call<List<ImageModel>> getAllImages(@Query("page") int page);
    }

    @Override
    public void getAllImages(Consumer<List<ImageModel>> onSuccess, Consumer<Throwable> onError) {
        // Get random page
        int randomPage = new Random().nextInt(10);
        service.getAllImages(randomPage).enqueue(new Callback<List<ImageModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<ImageModel>> call, @NonNull Response<List<ImageModel>> response) {
                onSuccess.accept(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<ImageModel>> call, @NonNull Throwable t) {
                onError.accept(t);
            }
        });
    }

}
