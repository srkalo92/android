package com.srdjan.android.data.remote.model;

import org.parceler.Parcel;

@Parcel
public class ImageModel {

    public String id;
    public String author;
    public String download_url;

    @Override
    public String toString() {
        return "ImageModel{" +
                "id='" + id + '\'' +
                ", author='" + author + '\'' +
                ", download_url='" + download_url + '\'' +
                '}';
    }

}
