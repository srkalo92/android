package com.srdjan.android.ui.view_image;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.srdjan.android.BuildConfig;
import com.srdjan.android.R;
import com.srdjan.android.data.remote.model.ImageModel;

import java.io.File;

public class ViewImagePresenter implements ViewImageContract.Presenter {

    public static final String FILE_PROVIDER_AUTHORITY = BuildConfig.APPLICATION_ID + ".fileprovider";
    private static final String TYPE_IMAGE_JPEG = "image/jpeg";

    private final ViewImageContract.View mView;

    public ViewImagePresenter(ViewImageContract.View view) {
        mView = view;
    }

    private ImageModel imageModel;

    @Override
    public void setImageModel(ImageModel imageModel) {
        this.imageModel = imageModel;
        mView.showImage(imageModel);
    }

    @Override
    public void shareImage() {
        shareImage(mView.getContext(), imageModel);
    }

    @Override
    public void shareUrl() {
        shareUrl(mView.getContext(), imageModel);
    }

    private void shareImage(Context context, ImageModel imageModel) {
        Glide.with(context)
                .asFile()
                .load(imageModel.download_url)
                .listener(new RequestListener<File>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<File> target, boolean isFirstResource) {
                        Toast.makeText(context, R.string.error_loading_image, Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(File resource, Object model, Target<File> target, DataSource dataSource, boolean isFirstResource) {
                        shareFile(context, resource);
                        return false;
                    }
                })
                .submit();
    }

    private void shareFile(Context context, File photo) {
        Uri photoUri = FileProvider.getUriForFile(context, FILE_PROVIDER_AUTHORITY, photo);
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_STREAM, photoUri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setType(TYPE_IMAGE_JPEG);
        context.startActivity(Intent.createChooser(shareIntent, context.getString(R.string.share_image)));
    }

    private void shareUrl(Context context, ImageModel imageModel) {
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, imageModel.author);
        shareIntent.putExtra(Intent.EXTRA_TEXT, imageModel.download_url);
        context.startActivity(Intent.createChooser(shareIntent, context.getString(R.string.share_url)));
    }

}
