package com.srdjan.android.data.local;

import androidx.annotation.Nullable;

public interface LocalDataSource {

    void setName(String name);

    @Nullable
    String getName();

}
