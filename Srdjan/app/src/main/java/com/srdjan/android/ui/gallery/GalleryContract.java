package com.srdjan.android.ui.gallery;

import com.srdjan.android.data.remote.model.ImageModel;
import com.srdjan.android.ui.base.BaseContract;

import java.util.List;

public interface GalleryContract {

    public interface View extends BaseContract.View {
        void showImages(List<ImageModel> images);
        void showImagesError(String error);
    }

    public interface Presenter extends BaseContract.Presenter {
        void loadGallery();
    }

}
