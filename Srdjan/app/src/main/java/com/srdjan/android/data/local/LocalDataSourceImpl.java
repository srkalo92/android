package com.srdjan.android.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.Nullable;

import com.srdjan.android.SrdjanApplication;
import com.srdjan.android.common.Constants;

import org.apache.commons.lang3.StringUtils;

public class LocalDataSourceImpl implements LocalDataSource {

    public static final String SHARED_PREFS_FILE_NAME = "sharedPreferences";

    private final SharedPreferences prefs;

    public LocalDataSourceImpl() {
        prefs = SrdjanApplication.getInstance().getSharedPreferences(SHARED_PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public void setName(String name) {
        if (StringUtils.isNotBlank(name)) {
            prefs.edit().putString(Constants.Prefs.NAME, name).apply();
        } else {
            prefs.edit().remove(Constants.Prefs.NAME).apply();
        }
    }

    @Nullable
    @Override
    public String getName() {
        return prefs.getString(Constants.Prefs.NAME, null);
    }

}
