package com.srdjan.android.ui.view_image;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.srdjan.android.R;
import com.srdjan.android.data.remote.model.ImageModel;
import com.srdjan.android.ui.base.BaseFragment;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class ViewImageFragment extends BaseFragment implements ViewImageContract.View {

    private static final String ARG_IMAGE_MODEL = "argImageModel";

    private final ViewImageContract.Presenter mPresenter = new ViewImagePresenter(this);

    @BindView(R.id.tvError)
    TextView tvError;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.imageView)
    ImageView imageView;

    public static ViewImageFragment newInstance(ImageModel imageModel) {
        ViewImageFragment viewImageFragment = new ViewImageFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_IMAGE_MODEL, Parcels.wrap(imageModel));
        viewImageFragment.setArguments(args);
        return viewImageFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_image, container, false);
        ButterKnife.bind(this, view);
        ImageModel imageModel = Parcels.unwrap(getArguments().getParcelable(ARG_IMAGE_MODEL));
        mPresenter.setImageModel(imageModel);
        return view;
    }

    @Override
    public void showImage(ImageModel image) {
        tvTitle.setText(image.author);

        Glide.with(getContext())
                .load(image.download_url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        showImageError(e.getMessage());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        tvError.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(imageView);
    }

    @Override
    public void showImageError(String error) {
        tvError.setVisibility(View.VISIBLE);
        tvError.setText(R.string.error_loading_image);
        Timber.e(error);
    }

    @OnClick(R.id.btnShareImage)
    public void onBtnShareImage() {
        mPresenter.shareImage();
    }

    @OnClick(R.id.btnShareUrl)
    public void onBtnShareUrl() {
        mPresenter.shareUrl();
    }

}
