package com.srdjan.android.ui.gallery;

import com.srdjan.android.data.Repository;

public class GalleryPresenter implements GalleryContract.Presenter {

    private final GalleryContract.View mView;

    private final Repository repository = new Repository();

    public GalleryPresenter(GalleryContract.View view) {
        mView = view;
    }

    @Override
    public void loadGallery() {
        repository.getAllImages(images -> {
            mView.showImages(images);
        }, error -> {
            error.printStackTrace();
            mView.showImagesError(error.getMessage());
        });
    }

}
