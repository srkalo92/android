package com.srdjan.android.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;

import com.srdjan.android.R;
import com.srdjan.android.common.Constants;
import com.srdjan.android.data.Repository;
import com.srdjan.android.ui.base.BaseActivity;
import com.srdjan.android.ui.edit_name.EditNameActivity;
import com.srdjan.android.ui.gallery.GalleryFragment;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {

    private final Repository repository = new Repository();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        updateName();

        // If the Activity has just been started
        if (savedInstanceState == null) {
            // Show the first Fragment
            GalleryFragment firstFragment = GalleryFragment.newInstance();
            switchToFragment(firstFragment);
        }
    }

    // Update displayed name in Action Bar (at the top of the screen)
    private void updateName() {
        String name = repository.getName();
        if (name == null) {
            name = getString(R.string.app_name);
        }
        getSupportActionBar().setTitle(name);
    }

    // Create menu button in Action Bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                // 'Edit Name' menu item clicked
                Intent intent = new Intent(this, EditNameActivity.class);
                intent.putExtra(Constants.Extras.NAME, repository.getName());
                startActivityForResult(intent, Constants.RequestCodes.GET_NAME);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.RequestCodes.GET_NAME:
                if (resultCode == RESULT_OK) {
                    // Handle result from EditNameActivity
                    String newName = data.getStringExtra(Constants.Extras.NAME);

                    // Save new name
                    repository.setName(newName);

                    // Refresh UI
                    updateName();
                }
                break;
        }
    }

}